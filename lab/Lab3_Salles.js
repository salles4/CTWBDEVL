
$(function(){
    $("#add").on("click", event =>{
        event.preventDefault();
        
        if (!$("#FORM")[0].reportValidity()){
            return;
        }
        
        let toAppend = ''
        toAppend = `
            <tr>
                <td>${$("#name").val()}</td>
                <td>${$("#email").val()}</td>
                <td>${$("#phone").val()}</td>
                <td>${$("#birth").val()}</td>
                <td>${$("#gender").val()}</td>
            </tr>
            `
            $("#tablebody").append(toAppend);

            $("#name").val("")
            $("#email").val("")
            $("#phone").val("")
            $("#birth").val("")
            
        
    })
})
