const step1 = document.getElementById("step1")
const step2 = document.getElementById("step2")
const step3 = document.getElementById("step3")

const steps = [step1, step2, step3]
steps.forEach(step => {
  step.addEventListener('mouseover', () => {
    step.querySelector('.options').style.display ="block"
  })
  step.addEventListener('mouseout', () => {
    step.querySelector('.options').style.display = "none"
  })
});
