const img = document.querySelector("#imgHover")
const cart = document.querySelector("#cart")
const cookieBanner = document.querySelector("#cookieBanner")
const cookieAccept = document.querySelector("#cookieAccept")

cart.addEventListener('click', () => {alert("Your cart is empty!")})
img.addEventListener('mouseover', ()=>{
  img.src = "img/Shadow_Isles_Crest.webp";
})
img.addEventListener('mouseout', ()=>{
  img.src = "img/Mount_Targon_Crest.webp";
})

cookieAccept.addEventListener("click", ()=>{
  cookieBanner.style.display = "none";
})