const savedData = localStorage.getItem("todo");
let globaltodo = [];
$(function(){
    $("#addTodo").on("click", ()=>{
        let newTodoObj = {};
        newTodoObj.title = $("#title").val();
        newTodoObj.description = $("#description").val();
        newTodoObj.due = $("#due").val();
        let todoArray = globaltodo;
        globaltodo.push(newTodoObj);
        console.log(globaltodo)
        list(globaltodo)
    })
})

function list(data){
    let toAppend = ``;
    data.forEach(todo => {
        toAppend += `
            <div class="card my-2">
                <div class="card-header card-title">
                    <span class="mx-1">${todo.title} -</span>
                    <a href="#edit"><i class="bi bi-pencil-square"></i></a>
                    <a href="#delete"><i class="bi bi-trash"></i></a>
                </div>
                <div class="card-body">
                    ${todo.description}
                </div>
                <div class="card-footer">
                    ${todo.due}
                </div>
            </div>
        `
    });
    $("#todoDiv").empty();
    $("#todoDiv").append(toAppend);
}